package mx.crosscorp.toodle;

/**
 * Interfaz de comportamiento general de presenters
 */
public interface BasePresenter {
    void start();
}
